class User < ApplicationRecord
  def name
    first_name + " " + last_name
  end

  def self.active
    User.where('active=?', true)
  end

end
