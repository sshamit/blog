FactoryGirl.define do
  factory :user do
    first_name 'Sam'
    last_name 'Harry'
    active false
  end
end